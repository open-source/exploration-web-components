# Sujet d'exploration - Web Components


L' objectif de cette exploration est d'essayer de répondre aux questions suivantes :

* Qu'est ce qu'un Web Component ? Benchmark des solutions existantes ?
* Web Component sans surcouches (framework) vs Web Component avec un framework, qui gagne ?
* Comment développer un Web Component ? Avantages et inconvénients de développer un Web Component ?
* Faire un retour des débats présents sur la toile : les avis divergent, mais pourquoi ?

*Exploration lancée en 2020 par Théo GROLLIER, étudiant en seconde année d'IUT à l'université de Nantes, dans le cadre de son stage de 2 mois au sein de l'incubateur des startups d'Etat Pôle emploi.*


## Introduction aux Web Components

Il s'agit d'éléments permettant de créer des composants d'interface graphique personnalisés et réutilisables par la suite (technologies web libres). Des technologies qui font partie du navigateur et ne nécessitent pas de bibliothèques externes comme JQuery ou Dojo. Une simple déclaration d'import dans une page HTML, et votre composant fonctionnera sur la page de votre site Web.

Les Web Components ont pour objectif la réutilisation de code en utilisant les technologies suivantes : 
- Custom Elements :  éléments HTML (balise) personnalisés permettant de créer sa propre balise HTML - [cf. caniuse.com - compatibilités avec les navigateurs](https://caniuse.com/#search=Custom%20Elements)
- HTML Templates : squelettes permettant de créer des éléments HTML instanciables (ne s'affiche pas par défaut) - [cf. caniuse.com - compatibilités avec les navigateurs](https://caniuse.com/#search=HTML%20Templates)
- Shadow DOM : permet d'encapsuler le JavaScript et le CSS des éléments, soit de créer un DOM "fantôme" où le css ne s'appliquera qu'au custom element et non à la page HTML entière par exemple - [cf. caniuse.com - compatibilités avec les navigateurs](https://caniuse.com/#search=Shadow%20DOM)


Un bon tutoriel expliquant les différentes utilisations possibles des Web Components : [Tutoriel JavaScript : les composants web - 09/2019](https://www.youtube.com/watch?v=lKm22TA5pzw&t=1241s)

## Benchmark des frameworks existants

Il existe différents frameworks pouvant faciliter la création de Web Components :

- Hybrids
- LitElement
- Polymer
- Skate.js
- Slim.js
- Stencil

**Quelques articles intéressants :**

* [Web Components: Recap and Latest Trends - State of the Web - 09/2019](https://www.inovex.de/blog/web-components-recap-trends/)
* [7 Tools for Developing Web Components in 2019 - 05/2019](https://blog.bitsrc.io/7-tools-for-developing-web-components-in-2019-1d5b7360654d)

## C'est parti, un peu de dév !!

Nous avons joué l'exercice de développer un Web Component sans surcouches, sans utiliser un framework. Ce Web Component nommé *RecapitulatifSituationFinanciere* s'inspire du prototype d'une future application qui sera développée dans le cadre de l'incubation de la startup Estime, une startup d'Etat Pôle emploi. Si vous êtes curieux, pour découvrir ce prototype, c'est par [ici](https://dernmr.invisionapp.com/console/Startup-estime-cka1geb32082x018d0mtv8oft/cka2lo2wd006s010h8jpmlutl/play).

Pour essayer de répondre à la question *« Web Component sans surcouches (framework) vs Web Component avec un framewok, qui gagne ? »*, ce même Web Component a été développé avec le framework Stencil.


IDE utilisé pour nos developpements : [Visual Studio Code](https://code.visualstudio.com/)


## Développement d'un Web Component sans framework (cf. web-component-sans-framework)


***`recapitulatif-situation-financiere.component.js`*** : classe représentant le composant Web et son comportement

```javascript
export default class RecapitulatifSituationFinanciere extends HTMLElement {
...
}
```

***`router.components.js`*** : classe permettant de déclarer les Web Component créés
```javascript
import RecapitulatifSituationFinanciere from './components/recapitulatif-situation-financiere.component.js'

customElements.define('recapitulatif-situation-financiere', RecapitulatifSituationFinanciere)
```

* customElements.define : définit un Web Component en tant que customElements. Il suffira ensuite d'intégrer la balise HTML dans le fichier .html dans lequel on souhaite utiliser le Web Component.

 ***`index.html`*** dans notre exemple :

```html
<recapitulatif-situation-financiere dates=`+data.date+` montantJour=`+data.montantJournalier+` are=`+data.are+` rsa=`+data.rsa+` total=`+data.total+` ></recapitulatif-situation-financiere>
```

### Run l'application en local

Utilisation du plugin VS Code [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer). 

Cela permet de lancer un serveur local de développement avec un rechargement automatique des pages. Un plugin très pratique :) 

Application accessible par défaut sur [http://localhost:5500](http://localhost:5500)


## Développement d'un Web Component avec le framework Stencil (cf. web-component-stencil)

Dans le cadre de cette exploration, nous allons utiliser [Stencil](https://stenciljs.com/docs/introduction), framework qui reste très utilisé et dont voici les promesses :

* helps developers and teams build and share custom components. Since Stencil generates standards-compliant Web Components, the components you build with Stencil will work with many popular frameworks right out of the box, and can even be used without a framework because they are just Web Components.
* Stencil was created by the Ionic Framework team to make our own component library faster, smaller, and compatible with all major frameworks.

### Utiliser Stencil


Pré-requis : Installer [NodeJS](https://nodejs.org/en/)


Créer un nouveau projet Stencil :
```
npm init stencil
```

Stencil vous propose de choisir un des starters suivants (option component choisie pour notre exemple) : 
```
? Pick a starter › - Use arrow-keys. Return to submit.

❯  ionic-pwa     Everything you need to build fast, production ready PWAs
   app           Minimal starter for building a Stencil app or website
   component     Collection of web components that can be used anywhere
```

Run l'application en local, accessible sur [http://localhost:3333](http://localhost:3333)
```
npm start
```

Build de l'application pour la prod :
```
npm run build
```

### Quelques explications


***`stencil.config.ts`*** : fichier de configuration Stencil. Plus d'infos dans la [documentation officielle]([doc officielle]([stencil.config.ts](https://stenciljs.com/docs/config/testing#stencil-config)).
```ts
import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'web-component-stencil',
  taskQueue: 'async',
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader'
    },
    {
      type: 'docs-readme'
    },
    {
      type: 'www',
      serviceWorker: null // disable service workers
    }
  ],
};
```

***`recapitulatif-situation-financiere.tsx`*** : classe définissant le comportement du Web Component
```ts
import { Component, Prop, h, EventEmitter, Event } from '@stencil/core';
import { format } from '../../utils/utils';


@Component({
    tag: 'recapitulatif-situation-financiere',
    styleUrl: 'recapitulatif-situation-financiere.css',
    shadow: true
  })
export class RecapitulatifSituationFinanciere {
  ...
}
```

* `shadow:true` : option permettant d'activer ou non le Shadow DOM 
* `recapitulatif-situation-financiere.css` : feuille de style du Web Component qui suit la loi du Shadow DOM et ne stylisera que celui-ci. 

*Méthodes de la classe :*

* `render()` : affichage graphique du Web Component
* `@Prop()` : variables permettant de récupérer les attributs du customElement. Exemple d'appel à une variable : `{this.mavariable}` ou `{this.mafonction()}`). 

***Intégration du component dans `index.html`*** :

```html
<recapitulatif-situation-financiere dates=`+data.date+` montantJour=`+data.montantJournalier+` are=`+data.are+` rsa=`+data.rsa+` total=`+data.total+` ></recapitulatif-situation-financiere>
```



## Un avis personnel sur l'utilisation ou non d'un framework

L'utilisation d'un framework facilite grandement le travail du développeur dans la création d'un Web Component. 

L'utilisation de Stencil est un bon moyen de créer des Web Components de manière simple et structurée avec :


* le concept de shadow DOM rendu très accessible, avec une option pour l'activer ou non
* un fichier css ne permettant de ne styliser que son Web Component
* coder en TypeScript : des développeurs pourront y trouver un certain confort. J'en fais parti ;)

Mais dans le cadre du développement d'un Web Component «simple» , la méthode sans framework, plus légère, peut aussi avoir ses avantages :


* un côté pratique dans la légèreté et la rapidité de développement
* des fichiers Javascript de plus petites tailles - un avantage qui peut être non négligeable dans certains contextes (faible connexion, etc...)
 

## Web Components : les avis sur le Web

L'utilisation ou non des Web Components pose de nombreux débats sur la toile. Si certains pensent que c'est un réel moyen rapide et facile de pouvoir créer et de partager un composant, d'autres dénoncent cette rapidité au détriment de la qualité.

Différents articles sont sources de débats. Certains développeurs estiment que les Web Components ne sont pas la meilleure réponse pour réaliser des interfaces web réutilisables et défendent le fait qu'il existe d'autres moyens beaucoup plus optimisés.

Quelques articles & discussions sur le sujet :

* [Maybe web components are not the future - 03/2020](https://dev.to/ryansolid/maybe-web-components-are-not-the-future-hfh)
* [The state of web components - 07/2019](https://medium.com/swlh/the-state-of-web-components-e3f746a22d75)
* [Why I don't use web components ? - 06/2019](https://dev.to/richharris/why-i-don-t-use-web-components-2cia) 
* [The problem with web components - 06/2019](https://adamsilver.io/articles/the-problem-with-web-components/) 

Un autre artice intéressant sur l'utilité ou non d'utiliser un framework, [web components, a frameworkless solution - 12/2019](https://www.hackdoor.io/articles/86pw2oNy/web-components-a-frameworkless-solution)

Bien évidemment, chacun est libre de se forger son opinion personnelle sur le sujet, l'objectif de ce projet étant seulement de vous aider à vous forger la vôtre ;)

## License

[MIT - code source in this project, feel free to use it !](https://choosealicense.com/licenses/mit/)

## 

