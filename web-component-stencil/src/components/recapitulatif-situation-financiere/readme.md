# recapitulatif-situation-financiere



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute        | Description | Type     | Default     |
| ---------------- | ---------------- | ----------- | -------- | ----------- |
| `are_journalier` | `are_journalier` |             | `number` | `undefined` |
| `are_mensuel`    | `are_mensuel`    |             | `number` | `undefined` |
| `date_are`       | `date_are`       |             | `string` | `undefined` |
| `rsa_mensuel`    | `rsa_mensuel`    |             | `number` | `undefined` |
| `total_mensuel`  | `total_mensuel`  |             | `number` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
