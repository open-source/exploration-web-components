import { Component, Prop, h } from '@stencil/core';
//import { format } from '../../utils/utils';


@Component({
    tag: 'recapitulatif-situation-financiere',
    styleUrl: 'recapitulatif-situation-financiere.css',
    shadow: true
  })
export class RecapitulatifSituationFinanciere {

  @Prop() date_are: string; 

  @Prop() are_journalier: number;

  @Prop() are_mensuel: number;

  @Prop() rsa_mensuel: number;

  @Prop() total_mensuel: number;


  
  public buildStringDate(): string {
      var str = this.date_are
      var splitted = str.split("-", 3)
      var mois = ""
      if(splitted[1] == "01") {
        mois = "janvier"
      }
      if(splitted[1] == "02") {
        mois = "février"
      }
      if(splitted[1] == "03") {
        mois = "mars"
      }
      if(splitted[1] == "04") {
        mois = "avril"
      }
      if(splitted[1] == "05") {
        mois = "mai"
      }
      if(splitted[1] == "06") {
        mois = "juin"
      }
      if(splitted[1] == "07") {
        mois = "juillet"
      }
      if(splitted[1] == "08") {
        mois = "août"
      }
      if(splitted[1] == "09") {
        mois = "septembre"
      }
      if(splitted[1] == "10") {
        mois = "octobre"
      }
      if(splitted[1] == "11") {
        mois = "novembre"
      }
      if(splitted[1] == "12") {
        mois = "décembre"
      }
      var final = splitted[2] + " " + mois + " " + splitted[0]

    return final
  }

    render() {
      return[
        <div class="rectangle-copy">     
        
        <p class="bonjour-voici-le-r">Récapitulatif de votre situation financière actuelle. </p>

        <div class="container1">
            <div class="group-5 mask-copy-3">
                <p class="mon-allocation-pole"> Mon allocation Pôle Emploi </p>
                <hr class="line-11-copy-8"></hr>
                <p class="are-allocation-de-r"> ARE (Allocation de Retour à l’Emploi)</p>
                <div class="dates">
                    <p class="text"> Sept 2018</p>
                    <p class="text-copy"> Sept 2020</p>
                </div>
                <div class="groupp rectangle-57-copy-5">
                    <div class="rectangle-57-copy"></div>
                </div>

                <hr class="line-11-copy"></hr>
                <p class="date-de-fin-dalloca">Date de fin d’allocation</p>
                <p class="date-fin-allocation-value">{this.buildStringDate()}</p>
                <hr class="line-11-copy-4"></hr>

                <p class="montant-journalier-d">Montant journalier de l’ARE</p>
                <p class="montant-journalier-value">{this.are_journalier} €</p>
            </div>
        
            <div class="group-7-copy mask-copy-32">
                <div class="rectangle"><p class="pourquoi-me-demande"> Pourquoi me demande t-on de prendre connaissance de ces informations ? </p> </div>
                <p class="ce-sont-les-informat">Ce sont les informations que nous allons utiliser pour faire la simulation.</p>
            </div>

        </div>

        <div class="container2">
            <div class="group-6 mask-copy-4">
                <p class="mes-ressources-actue"> Mes ressources actuelles </p>
                <hr class="line-11-copy-9"></hr>

                <p class="are-allocation-de-r2">ARE (Allocation de Retour à l’Emploi)</p>
                <p class="are-mensuel">{this.are_mensuel} € net/mois</p>
                <hr class="line-11-copy-5"></hr>

                <p class="rsa-revenu-de-solid">RSA (Revenu de Solidarité Active)</p>
                <p class="rsa-mensuel">{this.rsa_mensuel} € net/mois</p>
                <hr class="line-11-copy-6"></hr>

                <p class="total-mensuel">TOTAL MENSUEL</p>
                <p class="total-mensuel-value">{this.total_mensuel} € net/mois</p>
            </div>


            <div class="group-7-copy-2 mask-copy-33">
                <div class="rectangle"><p class="pourquoi-mon-allocat">Pourquoi mon allocation ( ARE ) est différente d’un mois sur l’autre ?</p></div>
                <p class="lallocation-du-mois">L’allocation du mois est calculée à partir du montant journalier et du nombre de jour dans le mois. Il varie donc tous les mois.</p>
            </div>
          </div>
          </div>
        
      ]
    }
    

}
