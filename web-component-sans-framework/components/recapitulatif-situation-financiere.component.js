export default class RecapitulatifSituationFinanciere extends HTMLElement {
    constructor(){
        super()
        this.root=this.attachShadow({mode: 'open'});
        var str = this.getAttribute("dates")
        var splitted = str.split("-")
        var mois = ""
        if(splitted[1] == "01") {
          mois = "Janvier"
        }
        if(splitted[1] == "02") {
          mois = "Février"
        }
        if(splitted[1] == "03") {
          mois = "Mars"
        }
        if(splitted[1] == "04") {
          mois = "Avril"
        }
        if(splitted[1] == "05") {
          mois = "Mai"
        }
        if(splitted[1] == "06") {
          mois = "Juin"
        }
        if(splitted[1] == "07") {
          mois = "Juillet"
        }
        if(splitted[1] == "08") {
          mois = "Août"
        }
        if(splitted[1] == "09") {
          mois = "Septembre"
        }
        if(splitted[1] == "10") {
          mois = "Octobre"
        }
        if(splitted[1] == "11") {
          mois = "Novembre"
        }
        if(splitted[1] == "12") {
          mois = "Décembre"
        }
        var final = splitted[2] + " " + mois + " " + splitted[0]
  
        
        var datas = {
          dates : final,
          montantJour : this.getAttribute("montantJour"),
          are : this.getAttribute("are"),
          rsa : this.getAttribute("rsa"),
          total : this.getAttribute("total")
        };


      
        this.root.innerHTML = ` 
        <style>
          
          .container1 {
            display:inline-block;
          }
          
          
          .container2 {
            display:inline-block;
          }
          
          .dates {
            display:inline-block;
            margin-top:-20px;
          
          }
          
        
          .rectangle-copy {
              height: 748px;
              width: 1024px;
              background-color: rgba(216,216,216,0.2);
              margin-left:17%;
            }
          
            .bonjour-voici-le-r {
              padding-top: 17px;
              margin-left: 114px;
              height: 60px;
              width: 892px;
              transform: rotate(360deg);
              color: #000000;
              font-family: Arial;
              font-size: 22px;
              font-weight: bold;
              letter-spacing: 0;
              line-height: 33px;
            }
          
            .rectangle-copy-2 {
              box-sizing: border-box;
              height: 10px;
              width: 10px;
              border: 1.8px solid #00B0FF;
              border-radius: 3px;
              background-color: #FFFFFF;
            }
          
            .rectangle-copy-5 {
              box-sizing: border-box;
              height: 10px;
              width: 10px;
              border: 1.8px solid #EE7013;
              border-radius: 3px;
              background-color: #FFFFFF;
            } 
          
            .rectangle-copy-4 {
              box-sizing: border-box;
              height: 10px;
              width: 10px;
              border: 1.8px solid #FFC400;
              border-radius: 3px;
              background-color: #FFFFFF;
            }
          
            .rectangle-copy-3 {
              box-sizing: border-box;
              height: 10px;
              width: 10px;
              border: 1.8px solid #158577;
              border-radius: 3px;
              background-color: #FFFFFF;
            }
          
            .rectangle-copy-2 {
              box-sizing: border-box;
              height: 10px;
              width: 10px;
              border: 1.8px solid #00B0FF;
              border-radius: 3px;
              background-color: #FFFFFF;
            }
          
            .groupp {
              height: 18px;
              width: 332px;
              margin-top:-17px;
            }
            /* Partie droite */
          
            .group-7-copy {
              height: 138px;
              width: 390px;
            }
          
            .rectangle {
              height: 58.5px;
              width: 390px;
              border-radius: 3px 3px 0 0;
              background-color: #24CCB8;
            }
          
            .group-7-copy-2 {
              height: 195px;
              width: 390px;
            }
          
            .pourquoi-mon-allocat {
              height: 38px;
              width: 320px;
              color: #FFFFFF;
              font-family: "Filson Pro";
              font-size: 16px;
              font-weight: bold;
              letter-spacing: 0;
              line-height: 19px;
              margin-left:13px;
              padding-top:8px;
            }
          
            .pourquoi-me-demande {
              height: 38px;
              width: 320px;
              color: #FFFFFF;
              font-family: "Filson Pro";
              font-size: 16px;
              font-weight: bold;
              letter-spacing: 0;
              line-height: 19px;
              padding-top:8px;
              margin-left:13px;
            }
          
            .lallocation-du-mois {
              height: 57px;
              width: 361px;
              color: #4A4A4A;
              font-family: "Filson Pro";
              font-size: 16px;
              letter-spacing: 0;
              line-height: 19px;
              margin-left:13px;
            }
          
            .ce-sont-les-informat {
              height: 38px;
              width: 361px;
              color: #4A4A4A;
              font-family: "Filson Pro";
              font-size: 16px;
              letter-spacing: 0;
              line-height: 19px;
              margin-left:13px;
           }
          
            .group-5 {
              height: 278px;
              width: 390px;
              float:left;
            }
          
            .text {
              height: 22px;
              width: 59px;
              color: #686868;
              font-family: "Filson Pro";
              font-size: 12px;
              letter-spacing: 0;
              line-height: 22px;
              float:left;
              margin-left:40px;
            }
          
            .text-copy {
              height: 22px;
              width: 62px;
              color: #686868;
              font-family: "Filson Pro";
              font-size: 12px;
              letter-spacing: 0;
              line-height: 22px;
              float:right;
              margin-left:211px;
            }
            .text-copy-2 {
              height: 22px;
              width: 22px;
              color: rgba(0,0,0,0.45);
              font-family: "Filson Pro";
              font-size: 12px;
              letter-spacing: 0;
              line-height: 22px;
              
            }
            /* Partie basse Retour suivant*/ 
          
            .group-52 {
              height: 30px;
              width: 106px;
              margin-left:372px;
            }
          
            .are-allocation-de-r {
              height: 19px;
              width: 284px;
              color: #686868;
              font-family: "Filson Pro";
              font-size: 16px;
              letter-spacing: 0;
              line-height: 19px;
              margin-top:15.58px;
              margin-left:40px;
            }
          
            .rectangle-57-copy {
              height: 18px;
              width: 228px;
              border-radius: 8px;
              background-color: #00B0FF;
            }
          
            .rectangle-57-copy-5 {
              height: 18px;
              width: 332px;
              border-radius: 8px;
              background-color: #F5F5F5;
              margin-left:40px;
              
            }
          
            .line-11-copy-4 {
              box-sizing: border-box;
              height: 2px;
              width: 390px;
              border: 1px solid #E8E8E8;
              margin-top:-5px;
            }
          
            .line-11-copy {
              box-sizing: border-box;
              height: 2px;
              width: 390px;
              border: 1px solid #E8E8E8;
              margin-top:20.21px;
            }
            .line-11-copy-8 {
              box-sizing: border-box;
              height: 2px;
              width: 390px;
              margin-top:-9.42px;
              border: 1px solid #E8E8E8;
            }
          
            .montant-journalier-value {
              height: 19px;
              width: 65px;
              color: #4A4A4A;
              font-family: "Filson Pro";
              font-size: 16px;
              font-weight: bold;
              letter-spacing: 0;
              line-height: 19px;
              margin-top:-18px;
              margin-left:40px;
            }
          
            .date-fin-allocation-value {
              height: 19px;
              width: 158px;
              color: #4A4A4A;
              font-family: "Filson Pro";
              font-size: 16px;
              font-weight: bold;
              letter-spacing: 0;
              line-height: 19px;
              margin-top:-16px;
              margin-left:40px;
            }
          
            .mon-allocation-pole {
              height: 19px;
              width: 216px;
              color: #4A4A4A;
              font-family: "Filson Pro";
              font-size: 16px;
              font-weight: bold;
              letter-spacing: 0;
              line-height: 19px;
              margin-top: 9px;
              margin-left: 37px;
            }
          
            .montant-journalier-d {
              height: 19px;
              width: 264px;
              color: #686868;
              font-family: "Filson Pro";
              font-size: 16px;
              letter-spacing: 0;
              line-height: 19px;
              text-transform: uppercase;
              margin-top:13.79px;
              margin-left:40px;
            }
          
          
            .date-de-fin-dalloca {
              height: 19px;
              width: 220px;
              color: #686868;
              font-family: "Filson Pro";
              font-size: 16px;
              letter-spacing: 0;
              line-height: 19px;
              margin-top:13.79px;
              margin-left:40px;
              text-transform: uppercase;
            }
          
            .mask-copy-3 {
              height: 278px;
              width: 390px;
              border-radius: 3px;
              background-color: #FFFFFF;
              margin-top: 27px;
              margin-left: 114px;
            }
          
            
            .mask-copy-32 {
              height: 138px;
              width: 390px;
              border-radius: 3px;
              background-color: #FFFFFF;
              margin-top: 27px;
              margin-left: 526px;
            }
          
            .mask-copy-33 {
              height: 195px;
              width: 390px;
              border-radius: 3px;
              background-color: #FFFFFF;
              margin-left:526px;
              margin-top:22px;
              
            }
          
          
            /* Deuxieme bloc */
          
            .group-6 {
              height: 251px;
              width: 390px;
              float:left;
            }
          
            .rsa-revenu-de-solid {
              height: 19px;
              width: 259px;
              color: #686868;
              font-family: "Filson Pro";
              font-size: 16px;
              letter-spacing: 0;
              line-height: 19px;
              margin-top:11.29px;
              margin-left:40px;
            }
          
          
          
            .are-allocation-de-r2 {
              height: 19px;
              width: 284px;
              color: #686868;
              font-family: "Filson Pro";
              font-size: 16px;
              letter-spacing: 0;
              line-height: 19px;
              margin-top:16.58px;
              margin-left:40px;
            }
          
            .line-11-copy-6 {
              box-sizing: border-box;
              height: 2px;
              width: 390px;
              border: 1px solid #E8E8E8;
              margin-top:11.21px;
            }
          
            .line-11-copy-5 {
              box-sizing: border-box;
              height: 2px;
              width: 390px;
              border: 1px solid #E8E8E8;
              margin-top:17.21px;
            }
          
            .line-11-copy-9 {
              box-sizing: border-box;
              height: 2px;
              width: 390px;
              border: 1px solid #E8E8E8;
              margin-top:-9.42px;
            }
          
            .total-mensuel-value {
              height: 19px;
              width: 162px;
              color: #4A4A4A;
              font-family: "Filson Pro";
              font-size: 16px;
              font-weight: bold;
              letter-spacing: 0;
              line-height: 19px;
              margin-top:-18px;
              margin-left:40px;
            }
          
            .rsa-mensuel {
              height: 19px;
              width: 142px;
              color: #4A4A4A;
              font-family: "Filson Pro";
              font-size: 16px;
              font-weight: bold;
              letter-spacing: 0;
              line-height: 19px;
              margin-top:-18px;
              margin-left:40px;
            }
          
            .are-mensuel {
              height: 19px;
              width: 122px;
              color: #4A4A4A;
              font-family: "Filson Pro";
              font-size: 16px;
              font-weight: bold;
              letter-spacing: 0;
              line-height: 19px;
              margin-top:-18px;
              margin-left:40px;
            }
          
            .mes-ressources-actue {
              height: 19px;
              width: 198px;
              color: #4A4A4A;
              font-family: "Filson Pro";
              font-size: 16px;
              font-weight: bold;
              letter-spacing: 0;
              line-height: 19px;
              margin-top: 9px;
              margin-left: 37px;
            }
          
          
            .total-mensuel {
              height: 19px;
              width: 132px;
              color: #686868;
              font-family: "Filson Pro";
              font-size: 16px;
              letter-spacing: 0;
              line-height: 19px;
              text-transform:uppercase;
              margin-top:13.79px;
              margin-left:40px;
            }
          
            .mask-copy-4 {
              height: 251px;
              width: 390px;
              border-radius: 3px;
              background-color: #FFFFFF;
              margin-left:114px;
              margin-top:22px;
            }
          


        </style>

        <div class="rectangle-copy">

        
        <p class="bonjour-voici-le-r">Récapitulatif de votre situation financière actuelle. </p>

        <div class="container1">
            <div class="group-5 mask-copy-3">
                <p class="mon-allocation-pole"> Mon allocation Pôle Emploi </p>
                <hr class="line-11-copy-8">
                <p class="are-allocation-de-r"> ARE (Allocation de Retour à l’Emploi)</p>
                <div class="dates">
                    <p class="text"> Sept 2018</p>
                    <p class="text-copy"> Sept 2020</p>
                </div>
                <div class="groupp rectangle-57-copy-5">
                    <div class="rectangle-57-copy"></div>
                </div>

                <hr class="line-11-copy">
                <p class="date-de-fin-dalloca">Date de fin d’allocation</p>
                <p class="date-fin-allocation-value">`+datas.dates+`</p>
                <hr class="line-11-copy-4">

                <p class="montant-journalier-d">Montant journalier de l’ARE</p>
                <p class="montant-journalier-value">`+datas.montantJour+` €</p>

            </div>
        
            <div class="group-7-copy mask-copy-32">
                <div class="rectangle"><p class="pourquoi-me-demande"> Pourquoi me demande t-on de prendre connaissance de ces informations ? </p> </div>
                <p class="ce-sont-les-informat">Ce sont les informations que nous allons utiliser pour faire la simulation.</p>
            </div>

        </div>

        <div class="container2">
            <div class="group-6 mask-copy-4">
                <p class="mes-ressources-actue"> Mes ressources actuelles </p>
                <hr class="line-11-copy-9">

                <p class="are-allocation-de-r2">ARE (Allocation de Retour à l’Emploi)</p>
                <p class="are-mensuel">`+datas.are+` € net/mois</p>
                <hr class="line-11-copy-5">

                <p class="rsa-revenu-de-solid">RSA (Revenu de Solidarité Active)</p>
                <p class="rsa-mensuel">`+datas.rsa+` € net/mois</p>
                <hr class="line-11-copy-6">

                <p class="total-mensuel">TOTAL MENSUEL</p>
                <p class="total-mensuel-value">`+datas.total+` € net/mois</p>
            </div>


            <div class="group-7-copy-2 mask-copy-33">
                <div class="rectangle"><p class="pourquoi-mon-allocat">Pourquoi mon allocation ( ARE ) est différente d’un mois sur l’autre ?</p></div>
                <p class="lallocation-du-mois">L’allocation du mois est calculée à partir du montant journalier et du nombre de jour dans le mois. Il varie donc tous les mois.</p>
            </div>
            </div>
        `

 
        

    }

  
}





